<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\KategoriProduk;
use Session;

class KategoriProdukController extends Controller
{
    public function index(){
        $kategori = KategoriProduk::all();
        return view('kategori_produk.index')->with(['data' => $kategori]);
    }

    public function add(){
        return view('kategori_produk.add');
    }

    public function create(Request $req)
    {
        $req->validate([
            'nama_kategori' => 'required|max:255',
        ]);
        $kategori = new KategoriProduk();
        $kategori->nama_kategori = $req->nama_kategori;
        if($kategori->save()){
            Session::flash('success','Data Berhasil ditambahkan');
            return redirect('admin/product-categories');
        }else{
            Session::flash('danger','Data Gagal ditambahkan');
            return redirect('admin/product-categories')->withInput();
        }
    }

    public function edit($id){
        $kategori = KategoriProduk::where('id',$id)->first();
        return view('kategori_produk.edit')->with(['kategori' => $kategori]);
    }

    public function update(Request $req, $id)
    {
        $req->validate([
            'nama_kategori' => 'required|max:255'
        ]);
        $kategori = KategoriProduk::where('id',$id)->update([
            'nama_kategori' => $req->nama_kategori
        ]);
        if($kategori){
            Session::flash('success','Data Berhasil diperbaharui');
            return redirect('admin/product-categories');
        }else{
            Session::flash('danger','Data Gagal diperbaharui');
            return redirect('admin/product-categories/edit/'.$id)->withInput();
        }
    }

    public function delete($id)
    {
        $kategori = KategoriProduk::where('id',$id)->delete();
            if($kategori){
                Session::flash('success','Data Berhasil dihapus');
                return redirect('admin/product-categories');
            }else{
                Session::flash('danger','Data Gagal dihapus');
                return redirect('admin/product-categories');
            }
    }
}
