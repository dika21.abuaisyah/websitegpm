<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FollowUp;

class FollowUpController extends Controller
{
    public function index(){
        $followup = FollowUp::get();
        return view('follow_up.index')->with(['data' => $followup]);
    }
}
