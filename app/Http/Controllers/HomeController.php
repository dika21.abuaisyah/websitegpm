<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Produk;
use App\Models\Berita;
use App\Models\Galeri;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $ju = User::get()->count();
        $jp = Produk::get()->count();
        $jb = Berita::get()->count();
        $jg = Galeri::get()->count();
        return view('home')->with([
            'ju' => $ju,
            'jp' => $jp,
            'jb' => $jb,
            'jg' => $jg,
        ]);
    }
}
