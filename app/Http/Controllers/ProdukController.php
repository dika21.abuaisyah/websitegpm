<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use App\Models\Produk;
use App\Models\KategoriProduk;
use Session;

class ProdukController extends Controller
{
    public function index(){
        $produk = Produk::with('kategori')->get();
        return view('produk.index')->with(['data' => $produk]);
    }

    public function show($id) {
        $produk = Produk::with('kategori')->findOrFail($id);
        return view('produk.show', compact('produk'));
    }

    public function detail($id)
    {
        $product = Produk::findOrFail($id);
        return response()->json($product);
    }

    public function add(){
        $kategori = KategoriProduk::all();
        return view('produk.add')->with(['kategori' => $kategori]);
    }

    public function create(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'nama_produk' => 'required|min:3|max:255',
            'harga' => 'required|min:0',
            'gambar' => 'required|mimes:jpeg,jpg,png,webp'
        ]);

        if($validator->fails()){
            return redirect('/admin/products/add')
            ->withErrors($validator)
            ->withInput();
        }else{
            $file = $req->gambar;
            $f = $file->getClientOriginalName();
            $ef = md5(microtime($f));

            $products = new Produk();
            $products->kategori_id = $req->kategori_id;
            $products->nama_produk = $req->nama_produk;
            $products->harga = $req->harga;
            $products->gambar = url('assets/image/products/'.$ef.".".$file->getClientOriginalExtension());
            $products->spesifikasi = $req->spesifikasi;
            if($products->save()){
                $file->move('assets/image/products/',$ef.".".$file->getClientOriginalExtension());
                Session::flash('success','Data Berhasil ditambahkan');
                return redirect('admin/products');
            }else{
                Session::flash('danger','Data Gagal ditambahkan');
                return redirect('admin/products/add')->withInput();
            }
        }
    }

    public function edit($id){
        $produk = Produk::where('id',$id)->first();
        $kategori = KategoriProduk::all();
        return view('produk.edit')->with(['produk' => $produk, 'kategori' => $kategori]);
    }

    public function update(Request $req, $id)
    {
        $validator = Validator::make($req->all(),[
            'nama_produk' => 'required|min:3|max:255',
            'harga' => 'required|min:0',
            'gambar' => 'required|mimes:jpeg,jpg,png,webp'
        ]);

        if($validator->fails()){
            return redirect('/admin/products/edit/'.$id)
            ->withErrors($validator)
            ->withInput();
        }else{
            $idProduct = Produk::find($id);
            if($idProduct){
                $file = $req->gambar;
                $f = $file->getClientOriginalName();
                $ef = md5(microtime($f));
                $produk = Produk::where('id',$id)->update([
                    'kategori_id' => $req->kategori_id,
                    'harga' => $req->harga,
                    'nama_produk' => $req->nama_produk,
                    'spesifikasi' => $req->spesifikasi,
                    'gambar' => url('assets/image/products/'.$ef.".".$file->getClientOriginalExtension())
                ]);
                if($produk > 0){
                    $file->move('assets/image/products/',$ef.".".$file->getClientOriginalExtension());
                    Session::flash('success','Data Berhasil diperbaharui');
                    return redirect('admin/products');
                }else{
                    Session::flash('danger','Data Gagal diperbaharui');
                    return redirect('admin/products/edit/'.$id)->withInput();
                }
            }else{
                Session::flash('danger','ID Tidak ditemukan');
                return redirect('admin/products/edit/'.$id)->withInput();
            }
        }
    }

    public function delete($id)
    {
        $produk = Produk::where('id',$id)->delete();
            if($produk){
                Session::flash('success','Data Berhasil dihapus');
                return redirect('admin/products');
            }else{
                Session::flash('danger','Data Gagal dihapus');
                return redirect('admin/products');
            }
    }
}
