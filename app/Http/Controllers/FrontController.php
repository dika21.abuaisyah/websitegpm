<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profil;
use App\Models\Produk;
use App\Models\KategoriProduk;
use App\Models\KategoriBerita;
use App\Models\Berita;
use App\Models\FollowUp;

class FrontController extends Controller
{
    public function index(){
        $profil1 = Profil::where('id',1)->first();
        $profil2 = Profil::where('id',2)->first();
        $kategori = KategoriProduk::get();
        $produk = Produk::get();
        $kategorib = KategoriBerita::get();
        $berita = Berita::get();
        return view('index')->with(['profil1' => $profil1, 'profil2' => $profil2, 'kategori' => $kategori, 'produk' => $produk, 'kategori_berita' => $kategorib, 'berita' => $berita]);
    }

    public function followup(Request $req){
        $fu = new FollowUp();
        $fu->nama = $req->nama;
        $fu->email = $req->email;
        $fu->no_wa = $req->no_wa;
        $fu->save();
        return redirect('/');
    }
}
