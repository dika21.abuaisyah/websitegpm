<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Galeri;
use Session;

class GaleriController extends Controller
{
    public function index(){
        $galeri = Galeri::get();
        return view('galeri.index')->with(['data' => $galeri]);
    }

    public function show($id) {
        $galeri = Galeri::findOrFail($id);
        return view('galeri.show', compact('galeri'));
    }

    public function add(){
        return view('galeri.add');
    }

    public function create(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'judul' => 'required|min:3|max:255',
            'gambar' => 'required|mimes:jpeg,jpg,png,webp'
        ]);

        if($validator->fails()){
            return redirect('/admin/gallery/add')
            ->withErrors($validator)
            ->withInput();
        }else{
            $file = $req->gambar;
            $f = $file->getClientOriginalName();
            $ef = md5(microtime($f));

            $gallery = new Galeri();
            $gallery->judul = $req->judul;
            $gallery->gambar = url('assets/image/gallery/'.$ef.".".$file->getClientOriginalExtension());
            if($gallery->save()){
                $file->move('assets/image/gallery/',$ef.".".$file->getClientOriginalExtension());
                Session::flash('success','Data Berhasil ditambahkan');
                return redirect('admin/gallery');
            }else{
                Session::flash('danger','Data Gagal ditambahkan');
                return redirect('admin/gallery/add')->withInput();
            }
        }
    }

    public function edit($id){
        $galeri = Galeri::where('id',$id)->first();
        return view('galeri.edit')->with(['galeri' => $galeri]);
    }

    public function update(Request $req, $id)
    {
        $validator = Validator::make($req->all(),[
            'judul' => 'required|min:3|max:255',
            'gambar' => 'required|mimes:jpeg,jpg,png,webp'
        ]);

        if($validator->fails()){
            return redirect('/admin/gallery/edit/'.$id)
            ->withErrors($validator)
            ->withInput();
        }else{
            $idgallery = Galeri::find($id);
            if($idgallery){
                $file = $req->gambar;
                $f = $file->getClientOriginalName();
                $ef = md5(microtime($f));
                $galeri = Galeri::where('id',$id)->update([
                    'judul' => $req->judul,
                    'gambar' => url('assets/image/gallery/'.$ef.".".$file->getClientOriginalExtension())
                ]);
                if($galeri > 0){
                    $file->move('assets/image/gallery/',$ef.".".$file->getClientOriginalExtension());
                    Session::flash('success','Data Berhasil diperbaharui');
                    return redirect('admin/gallery');
                }else{
                    Session::flash('danger','Data Gagal diperbaharui');
                    return redirect('admin/gallery/edit/'.$id)->withInput();
                }
            }else{
                Session::flash('danger','ID Tidak ditemukan');
                return redirect('admin/gallery/edit/'.$id)->withInput();
            }
        }
    }

    public function delete($id)
    {
        $galeri = Galeri::where('id',$id)->delete();
            if($galeri){
                Session::flash('success','Data Berhasil dihapus');
                return redirect('admin/gallery');
            }else{
                Session::flash('danger','Data Gagal dihapus');
                return redirect('admin/gallery');
            }
    }
}
