<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\KategoriBerita;
use Session;

class KategoriBeritaController extends Controller
{
    public function index(){
        $kategori = KategoriBerita::all();
        return view('kategori_Berita.index')->with(['data' => $kategori]);
    }

    public function add(){
        return view('kategori_Berita.add');
    }

    public function create(Request $req)
    {
        $req->validate([
            'nama_kategori' => 'required|max:255',
        ]);
        $kategori = new KategoriBerita();
        $kategori->nama_kategori = $req->nama_kategori;
        if($kategori->save()){
            Session::flash('success','Data Berhasil ditambahkan');
            return redirect('admin/news-categories');
        }else{
            Session::flash('danger','Data Gagal ditambahkan');
            return redirect('admin/news-categories')->withInput();
        }
    }

    public function edit($id){
        $kategori = KategoriBerita::where('id',$id)->first();
        return view('kategori_Berita.edit')->with(['kategori' => $kategori]);
    }

    public function update(Request $req, $id)
    {
        $req->validate([
            'nama_kategori' => 'required|max:255'
        ]);
        $kategori = KategoriBerita::where('id',$id)->update([
            'nama_kategori' => $req->nama_kategori
        ]);
        if($kategori){
            Session::flash('success','Data Berhasil diperbaharui');
            return redirect('admin/news-categories');
        }else{
            Session::flash('danger','Data Gagal diperbaharui');
            return redirect('admin/news-categories/edit/'.$id)->withInput();
        }
    }

    public function delete($id)
    {
        $kategori = KategoriBerita::where('id',$id)->delete();
            if($kategori){
                Session::flash('success','Data Berhasil dihapus');
                return redirect('admin/news-categories');
            }else{
                Session::flash('danger','Data Gagal dihapus');
                return redirect('admin/news-categories');
            }
    }
}
