<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Profil;
use Session;

class ProfilController extends Controller
{
    public function index_profile(){
        $profil = Profil::where('id','1')->first();
        return view('profil.profile')->with(['profil' => $profil]);
    }

    public function update_profile(Request $req){
        $profil = Profil::where('id','1')->update([
            'isi' => $req->isi
        ]);
        if($profil > 0){
            Session::flash('success','Data Berhasil diperbaharui');
            return redirect('admin/company-profile');
        }else{
            Session::flash('danger','Data Gagal diperbaharui');
            return redirect('admin/company-profile')->withInput();
        }
    }

    public function index_visi(){
        $profil = Profil::where('id','2')->first();
        return view('profil.visimisi')->with(['profil' => $profil]);
    }

    public function update_visi(Request $req){
        $profil = Profil::where('id','2')->update([
            'isi' => $req->isi
        ]);
        if($profil > 0){
            Session::flash('success','Data Berhasil diperbaharui');
            return redirect('admin/vision-mission');
        }else{
            Session::flash('danger','Data Gagal diperbaharui');
            return redirect('admin/vision-mission')->withInput();
        }
    }
}
