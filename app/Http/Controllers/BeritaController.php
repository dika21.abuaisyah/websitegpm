<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Berita;
use App\Models\KategoriBerita;
use Session;

class BeritaController extends Controller
{
    public function index(){
        $berita = Berita::with('kategori')->get();
        return view('berita.index')->with(['data' => $berita]);
    }

    public function show($id) {
        $berita = Berita::with('kategori')->findOrFail($id);
        return view('berita.show', compact('berita'));
    }

    public function detail($id)
    {
        $berita = Berita::findOrFail($id);
        return response()->json($berita);
    }

    public function add(){
        $kategori = KategoriBerita::all();
        return view('berita.add')->with(['kategori' => $kategori]);
    }

    public function create(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'judul' => 'required|min:3|max:255',
            'isi' => 'required|min:0',
            'thumbnail' => 'required|mimes:jpeg,jpg,png,webp'
        ]);

        if($validator->fails()){
            return redirect('/admin/news/add')
            ->withErrors($validator)
            ->withInput();
        }else{
            $file = $req->thumbnail;
            $f = $file->getClientOriginalName();
            $ef = md5(microtime($f));

            $news = new Berita();
            $news->kategori_id = $req->kategori_id;
            $news->judul = $req->judul;
            $news->isi = $req->isi;
            $news->thumbnail = url('assets/image/news/'.$ef.".".$file->getClientOriginalExtension());
            if($news->save()){
                $file->move('assets/image/news/',$ef.".".$file->getClientOriginalExtension());
                Session::flash('success','Data Berhasil ditambahkan');
                return redirect('admin/news');
            }else{
                Session::flash('danger','Data Gagal ditambahkan');
                return redirect('admin/news/add')->withInput();
            }
        }
    }

    public function edit($id){
        $berita = Berita::where('id',$id)->first();
        $kategori = KategoriBerita::all();
        return view('berita.edit')->with(['berita' => $berita, 'kategori' => $kategori]);
    }

    public function update(Request $req, $id)
    {
        $validator = Validator::make($req->all(),[
            'judul' => 'required|min:3|max:255',
            'isi' => 'required|min:0',
            'thumbnail' => 'required|mimes:jpeg,jpg,png,webp'
        ]);

        if($validator->fails()){
            return redirect('/admin/news/edit/'.$id)
            ->withErrors($validator)
            ->withInput();
        }else{
            $idnews = Berita::find($id);
            if($idnews){
                $file = $req->thumbnail;
                $f = $file->getClientOriginalName();
                $ef = md5(microtime($f));
                $berita = Berita::where('id',$id)->update([
                    'kategori_id' => $req->kategori_id,
                    'isi' => $req->isi,
                    'judul' => $req->judul,
                    'thumbnail' => url('assets/image/news/'.$ef.".".$file->getClientOriginalExtension())
                ]);
                if($berita > 0){
                    $file->move('assets/image/news/',$ef.".".$file->getClientOriginalExtension());
                    Session::flash('success','Data Berhasil diperbaharui');
                    return redirect('admin/news');
                }else{
                    Session::flash('danger','Data Gagal diperbaharui');
                    return redirect('admin/news/edit/'.$id)->withInput();
                }
            }else{
                Session::flash('danger','ID Tidak ditemukan');
                return redirect('admin/news/edit/'.$id)->withInput();
            }
        }
    }

    public function delete($id)
    {
        $berita = Berita::where('id',$id)->delete();
            if($berita){
                Session::flash('success','Data Berhasil dihapus');
                return redirect('admin/news');
            }else{
                Session::flash('danger','Data Gagal dihapus');
                return redirect('admin/news');
            }
    }
}
