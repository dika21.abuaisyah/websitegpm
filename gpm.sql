-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 29, 2024 at 04:38 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gpm`
--

-- --------------------------------------------------------

--
-- Table structure for table `beritas`
--

CREATE TABLE `beritas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kategori_id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `beritas`
--

INSERT INTO `beritas` (`id`, `kategori_id`, `judul`, `isi`, `thumbnail`, `created_at`, `updated_at`) VALUES
(1, 1, 'Honda EM1 E: Ternyata Pakai Aki Seperti Honda BeAT', '<p>Motor listrik Honda EM1 e:&nbsp; masih menggunakan aki atau baterai 12 volt. Fungsinya untuk menghidupkan speedometer, lampu, stoplamp dan juga lampu sein. Kalau pada motor berbahan baku bensin, pengisian aki atau baterai 12 volt berasal dari spul atau generator. Sementara pada motor listrik seperti Honda EM1 e:, pengisian aki berasal dari baterai utama atau&nbsp; Honda Mobile Power Pack. Tegangannya diubah melalui komponen down regulator agar Listrik bisa disimpan di aki. Secara sederhana, komponen down regulator pada motor listrik Honda EM1 e: berfungsi mirip dengan kiprok pada motor bensin. Soalnya, selain mengatur arus dari spul, kiprok juga mengubah arus yang berasal dari spul atau generator menjadi 12 volt agar bisa disimpan di aki. Jenis aki yang digunakan Honda EM1 e: sesungguhnya sama seperti aki yang digunakan oleh Honda BeAT. Di mana aki tersebut seperti aki motor pada umumnya, dengan spesifikasi 12 volt 3 ampere hour. Sementara Honda EM1 e: menggunakan aki dengan tipe YTZ4A, sama dengan yang digunakan Honda BeAT.<br></p>', 'http://localhost:8000/assets/image/news/3738e84e9c98d6af1cf5c4b736bc00e8.webp', '2024-05-16 20:52:40', '2024-05-16 20:52:40');

-- --------------------------------------------------------

--
-- Table structure for table `follow_ups`
--

CREATE TABLE `follow_ups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `no_wa` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `follow_ups`
--

INSERT INTO `follow_ups` (`id`, `nama`, `email`, `no_wa`, `created_at`, `updated_at`) VALUES
(1, 'Joko', 'joko@gmail.com', '08123123123', '2024-05-29 02:33:22', '2024-05-29 02:33:22');

-- --------------------------------------------------------

--
-- Table structure for table `galeris`
--

CREATE TABLE `galeris` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galeris`
--

INSERT INTO `galeris` (`id`, `judul`, `gambar`, `created_at`, `updated_at`) VALUES
(1, 'Galeri 1', 'http://localhost:8000/assets/image/gallery/8f1b7915fe79f80aa1dec00c6e404a38.jpg', '2024-05-16 21:06:40', '2024-05-16 21:06:40');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_beritas`
--

CREATE TABLE `kategori_beritas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kategori` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori_beritas`
--

INSERT INTO `kategori_beritas` (`id`, `nama_kategori`, `created_at`, `updated_at`) VALUES
(1, 'Tips', '2024-05-16 20:40:29', '2024-05-16 20:50:04');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_produks`
--

CREATE TABLE `kategori_produks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kategori` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori_produks`
--

INSERT INTO `kategori_produks` (`id`, `nama_kategori`, `created_at`, `updated_at`) VALUES
(1, 'Matic', '2024-05-16 18:43:01', '2024-05-16 18:58:39'),
(2, 'CUB', '2024-05-16 18:43:52', '2024-05-16 18:43:52'),
(3, 'Sport', '2024-05-16 18:43:58', '2024-05-16 18:43:58'),
(4, 'EV', '2024-05-16 18:44:03', '2024-05-16 18:44:03'),
(5, 'Big Bike', '2024-05-16 18:45:27', '2024-05-16 18:45:27');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(3, '2024_05_14_202328_create_kategori_produks_table', 1),
(4, '2024_05_14_202335_create_produks_table', 1),
(5, '2024_05_14_202345_create_galeris_table', 1),
(6, '2024_05_14_202353_create_kategori_beritas_table', 1),
(7, '2024_05_14_202359_create_beritas_table', 1),
(8, '2024_05_14_202419_create_follow_ups_table', 1),
(9, '2024_05_14_202829_create_profils_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `produks`
--

CREATE TABLE `produks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kategori_id` bigint(20) UNSIGNED NOT NULL,
  `nama_produk` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL,
  `spesifikasi` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `produks`
--

INSERT INTO `produks` (`id`, `kategori_id`, `nama_produk`, `gambar`, `harga`, `spesifikasi`, `created_at`, `updated_at`) VALUES
(1, 1, 'Honda BeAT Street', 'http://localhost:8000/assets/image/products/e641cf6df86c8db5110ad0109a677537.webp', 19523000, '<div class=\"row pl-4\" style=\"margin-right: -15px; margin-left: -15px; font-family: Poppins, sans-serif; background-color: rgb(251, 251, 251);\"><div class=\"col-pc-35 font-weight-600 text-color\" style=\"flex: 0 0 35%; max-width: 35%; color: rgb(65, 65, 65); font-weight: 600 !important;\"><p>Panjang x Lebar x tinggi</p></div><div class=\"col-pc-5\" style=\"flex: 0 0 5%; max-width: 5%;\">:</div><div class=\"col-pc-60\" style=\"flex: 0 0 60%; max-width: 60%;\"><p>1877 x 742 x 1030 mm</p></div></div><div class=\"row pl-4\" style=\"margin-right: -15px; margin-left: -15px; font-family: Poppins, sans-serif; background-color: rgb(251, 251, 251);\"><div class=\"col-pc-35 font-weight-600 text-color\" style=\"flex: 0 0 35%; max-width: 35%; color: rgb(65, 65, 65); font-weight: 600 !important;\"><p>Jarak Sumbu Roda</p></div><div class=\"col-pc-5\" style=\"flex: 0 0 5%; max-width: 5%;\">:</div><div class=\"col-pc-60\" style=\"flex: 0 0 60%; max-width: 60%;\"><p>1251 mm</p></div></div><div class=\"row pl-4\" style=\"margin-right: -15px; margin-left: -15px; font-family: Poppins, sans-serif; background-color: rgb(251, 251, 251);\"><div class=\"col-pc-35 font-weight-600 text-color\" style=\"flex: 0 0 35%; max-width: 35%; color: rgb(65, 65, 65); font-weight: 600 !important;\"><p>Jarak Terendah Ke Tanah</p></div><div class=\"col-pc-5\" style=\"flex: 0 0 5%; max-width: 5%;\">:</div><div class=\"col-pc-60\" style=\"flex: 0 0 60%; max-width: 60%;\"><p>145 mm</p></div></div><div class=\"row pl-4\" style=\"margin-right: -15px; margin-left: -15px; font-family: Poppins, sans-serif; background-color: rgb(251, 251, 251);\"><div class=\"col-pc-35 font-weight-600 text-color\" style=\"flex: 0 0 35%; max-width: 35%; color: rgb(65, 65, 65); font-weight: 600 !important;\"><p>Ketinggian Tempat Duduk</p></div><div class=\"col-pc-5\" style=\"flex: 0 0 5%; max-width: 5%;\">:</div><div class=\"col-pc-60\" style=\"flex: 0 0 60%; max-width: 60%;\"><p>740 mm</p></div></div><div class=\"row pl-4\" style=\"margin-right: -15px; margin-left: -15px; font-family: Poppins, sans-serif; background-color: rgb(251, 251, 251);\"><div class=\"col-pc-35 font-weight-600 text-color\" style=\"flex: 0 0 35%; max-width: 35%; color: rgb(65, 65, 65); font-weight: 600 !important;\"><p>Berat Kosong</p></div><div class=\"col-pc-5\" style=\"flex: 0 0 5%; max-width: 5%;\">:</div><div class=\"col-pc-60\" style=\"flex: 0 0 60%; max-width: 60%;\"><p>95 Kg</p></div></div>', '2024-05-16 19:39:59', '2024-05-16 20:11:55');

-- --------------------------------------------------------

--
-- Table structure for table `profils`
--

CREATE TABLE `profils` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profils`
--

INSERT INTO `profils` (`id`, `judul`, `isi`, `created_at`, `updated_at`) VALUES
(1, 'PT Gratia Plenamas Motor', '<p style=\"text-align: center;\"><span id=\"docs-internal-guid-af9ae23d-7fff-a63f-26b9-5b84a410184d\"><span style=\"font-size: 12pt; font-family: &quot;Times New Roman&quot;; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; font-variant-alternates: normal; font-variant-position: normal; vertical-align: baseline; white-space-collapse: preserve;\"><span style=\"font-weight: bolder;\">Visi</span></span></span></p><p style=\"text-align: center;\"><span id=\"docs-internal-guid-af9ae23d-7fff-a63f-26b9-5b84a410184d\"><span style=\"font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; font-variant-alternates: normal; font-variant-position: normal; vertical-align: baseline; white-space-collapse: preserve;\">Menjadi Group Dealer Otomotif Terbaik Pilihan Customer di Sumatera Selatan Dengan Menerapkan Totalitas Pelayanan Kelas Dunia Dan Kompetensi SDM Yang Handal</span></span></p><p style=\"text-align: center;\"><span id=\"docs-internal-guid-af9ae23d-7fff-a63f-26b9-5b84a410184d\"><span style=\"font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; font-variant-alternates: normal; font-variant-position: normal; vertical-align: baseline; white-space-collapse: preserve;\"><span style=\"font-weight: bolder;\">Misi</span></span></span></p><p style=\"text-align: center;\"><span id=\"docs-internal-guid-af9ae23d-7fff-a63f-26b9-5b84a410184d\"><font color=\"#000000\" face=\"Times New Roman, serif\"><span style=\"white-space-collapse: preserve;\">Berkomitmen memberikan Kualitas Produk &amp; Service yang terbaik untuk Pelanggan secara konsisten dengan cara meningkatkan kompetensi dan juga memaksimalkan potensi SDM GPM Honda, agar dapat memberikan Customer Experience yang tidak terlupakan</span></font><br></span></p>', NULL, '2024-05-16 21:27:02'),
(2, 'Visi Misi PT Gratia Plenamas Motor', '<h2 class=\"\" style=\"font-family: &quot;Source Sans Pro&quot;, -apple-system, &quot;system-ui&quot;, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; color: rgb(33, 37, 41); text-align: center;\"><font color=\"#000000\" face=\"Times New Roman, serif\"><span style=\"white-space-collapse: preserve;\"><span style=\"font-weight: bolder;\">Visi</span></span></font></h2><p style=\"text-align: center;\"><span id=\"docs-internal-guid-af9ae23d-7fff-a63f-26b9-5b84a410184d\"><span style=\"font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; font-variant-alternates: normal; font-variant-position: normal; vertical-align: baseline; white-space-collapse: preserve;\">Menjadi Group Dealer Otomotif Terbaik Pilihan Customer di Sumatera Selatan Dengan Menerapkan Totalitas Pelayanan Kelas Dunia Dan Kompetensi SDM Yang Handal</span></span></p><p style=\"text-align: center;\"><span id=\"docs-internal-guid-af9ae23d-7fff-a63f-26b9-5b84a410184d\"><span style=\"font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; font-variant-alternates: normal; font-variant-position: normal; vertical-align: baseline; white-space-collapse: preserve;\"><br></span></span></p><h2 style=\"text-align: center;\" class=\"\"><font color=\"#000000\" face=\"Times New Roman, serif\"><span style=\"white-space-collapse: preserve;\"><b>Misi</b></span></font></h2><p style=\"text-align: center;\"><span id=\"docs-internal-guid-af9ae23d-7fff-a63f-26b9-5b84a410184d\"><font color=\"#000000\" face=\"Times New Roman, serif\"><span style=\"white-space-collapse: preserve;\">Berkomitmen memberikan Kualitas Produk &amp; Service yang terbaik untuk Pelanggan secara konsisten dengan cara meningkatkan kompetensi dan juga memaksimalkan potensi SDM GPM Honda, agar dapat memberikan Customer Experience yang tidak terlupakan</span></font><br></span></p>', NULL, '2024-05-16 21:28:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Riki Ronaldo', 'rikironaldo@gmail.com', '$2y$12$tesUyUSk7TquCN8hYSsVJuAmw8VYvludLOnN1jUsvzQsYwdmr3fZ.', '2024-05-14 14:05:10', '2024-05-14 14:05:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beritas`
--
ALTER TABLE `beritas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `beritas_kategori_id_foreign` (`kategori_id`);

--
-- Indexes for table `follow_ups`
--
ALTER TABLE `follow_ups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeris`
--
ALTER TABLE `galeris`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_beritas`
--
ALTER TABLE `kategori_beritas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_produks`
--
ALTER TABLE `kategori_produks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produks`
--
ALTER TABLE `produks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produks_kategori_id_foreign` (`kategori_id`);

--
-- Indexes for table `profils`
--
ALTER TABLE `profils`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beritas`
--
ALTER TABLE `beritas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `follow_ups`
--
ALTER TABLE `follow_ups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `galeris`
--
ALTER TABLE `galeris`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kategori_beritas`
--
ALTER TABLE `kategori_beritas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kategori_produks`
--
ALTER TABLE `kategori_produks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `produks`
--
ALTER TABLE `produks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `profils`
--
ALTER TABLE `profils`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `beritas`
--
ALTER TABLE `beritas`
  ADD CONSTRAINT `beritas_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori_beritas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `produks`
--
ALTER TABLE `produks`
  ADD CONSTRAINT `produks_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori_produks` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
