<?php

return [

    'main_navigation' => 'MAIN NAVIGATION',
    'blog' => 'Blog',
    'pages' => 'Pages',
    'account_settings' => 'ACCOUNT SETTINGS',
    'profile' => 'Profil',
    'change_password' => 'Change Password',
    'multilevel' => 'Multi Level',
    'level_one' => 'Level 1',
    'level_two' => 'Level 2',
    'level_three' => 'Level 3',
    'labels' => 'LABELS',
    'important' => 'Important',
    'warning' => 'Warning',
    'information' => 'Information',
    'dashboard' => 'Dashboard',
    'user' => 'Pengguna',
    'category_product' => 'Kategori Produk',
    'product' => 'Produk',
    'category_news' => 'Kategori Berita',
    'news' => 'Berita',
    'gallery' => 'Galeri',
    'follow_up' => 'Follow Up',
    'company_profile' => 'Profil Perusahaan',
    'vision_and_mission' => 'Visi Misi',
];
