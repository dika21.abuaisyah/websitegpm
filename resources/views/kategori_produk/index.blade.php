@extends('adminlte::page')
@section('title', 'Data Kategori Produk')
@section('content_header')
<h1>Data Kategori Produk</h1>
@stop
@section('content')
<div class="row">
  <div class="col-12">
    <div class="form-group">
      <a href="{{ url('admin/product-categories/add') }}" class="btn btn-primary">Tambah Data</a>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @elseif($message = Session::get('danger'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @endif

    @php
    $heads = [
    'No',
    'Nama',
    ['label' => 'Actions', 'no-export' => true, 'width' => 25],
    ];
    @endphp
    <x-adminlte-datatable id="table2" :heads="$heads" striped hoverable>
    @foreach($data as $kategori)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $kategori->nama_kategori }}</td>
        <td><a href="{{ url('admin/product-categories/edit/'.$kategori->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" onclick="return remove({{ $kategori->id }});" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Hapus</a>
      </tr>
    @endforeach
    </x-adminlte-datatable>
  </div>
</div>
@stop
@section('js')
<script>
function remove(e){
	Swal.fire({
	title: 'Hapus ?',
	text: "Jika dihapus, data tidak bisa dikembalikan!",
	icon: 'warning',
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	confirmButtonText: 'Ya, hapus !!',
	cancelButtonText: 'Batal',
	container: 'container-class'
	})
	.then((result) => {
		if (result.value) {
			window.location.href = "{{ url('admin/product-categories/delete') }}/"+e ;
		}
	})
}
</script>
@stop