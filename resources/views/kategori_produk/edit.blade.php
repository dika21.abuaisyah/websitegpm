@extends('adminlte::page')

@section('title', 'Edit Data Kategori Produk')

@section('content_header')
<h1> Edit Data Kategori Produk</h1>
@stop

@section('content')
@if ($message = Session::get('danger'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif
<a href="{{ url('/admin/product-categories') }}" class="btn btn-warning mb-3"><i class="fa fa-arrow-left"></i> Kembali</a>
<div class="card col-12 col-md-6">
    <div class="card-body">
        <form action="{{ route('product-categories/update',['id'=>$kategori->id]) }}" method="post">
            @csrf
            <div class="form-group">
                <label for="nama_kategori">Nama Kategori Produk<span class="text-danger">*</span></label>
                <input id="nama_kategori" class="form-control @if($errors->has('nama_kategori')) is-invalid @endif" type="text" name="nama_kategori" placeholder="Nama Kategori Produk" value="{{ $kategori->nama_kategori ?? old('nama_kategori') }}" required>
                @if($errors->has('nama_kategori'))
                <small class="text-danger">{{ $errors->first('nama_kategori') }}</small>
                @endif
            </div>
            <div class="form-group">
                <button class="btn btn-primary">Edit</button>
            </div>
        </form>
    </div>
</div>
@stop

