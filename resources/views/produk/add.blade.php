@extends('adminlte::page')

@section('title', 'Tambah Data Produk')

@section('content_header')
<h1> Tambah Data Produk</h1>
@stop

@section('content')
@if ($message = Session::get('danger'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif
<a href="{{ url('/admin/products') }}" class="btn btn-warning mb-3"><i class="fa fa-arrow-left"></i> Kembali</a>
<div class="card col-12 col-md-6">
    <div class="card-body">
        <form action="{{ url('/admin/products/create') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="kategori_id">Kategori Produk<span class="text-danger">*</span></label>
                <x-adminlte-select2 name="kategori_id"
                    igroup-size="md" data-placeholder="Select an option...">
                    @foreach($kategori as $k)
                    <option value="{{ $k->id }}">{{ $k->nama_kategori }}</option>
                    @endforeach
                </x-adminlte-select2>
                @if($errors->has('kategori_id'))
                <small class="text-danger">{{ $errors->first('kategori_id') }}</small>
                @endif                
            </div>
            <div class="form-group">
                <label for="nama_produk">Nama Produk<span class="text-danger">*</span></label>
                <input id="nama_produk" class="form-control @if($errors->has('nama_produk')) is-invalid @endif" type="text" name="nama_produk" value="{{ old('nama_produk') }}" placeholder="Nama Kategori Produk" required>
                @if($errors->has('nama_produk'))
                <small class="text-danger">{{ $errors->first('nama_produk') }}</small>
                @endif
            </div>
            <div class="form-group">
                <label for="harga">Harga Produk<span class="text-danger">*</span></label>
                <input id="harga" class="form-control @if($errors->has('harga')) is-invalid @endif" type="number" name="harga" value="{{ old('harga') }}" placeholder="Harga Produk" required>
                @if($errors->has('harga'))
                <small class="text-danger">{{ $errors->first('harga') }}</small>
                @endif
            </div>
            <div class="form-group">
                <label for="gambar">Gambar Produk<span class="text-danger">*</span></label>
                <input id="gambar" class="form-control @if($errors->has('gambar')) is-invalid @endif" type="file" name="gambar" value="{{ old('gambar') }}" placeholder="Gambar Produk" required>
                @if($errors->has('gambar'))
                <small class="text-danger">{{ $errors->first('gambar') }}</small>
                @endif
            </div>
            @php
            $config = [
                "height" => "150"
            ];
            @endphp
            <div class="form-group">
                <label for="spesifikasi">Spesifikasi Produk</label>
                <x-adminlte-text-editor name="spesifikasi" :config="$config" placeholder="Spesifikasi Produk"/>
                @if($errors->has('spesifikasi'))
                <small class="text-danger">{{ $errors->first('spesifikasi') }}</small>
                @endif
            </div>
            <div class="form-group">
                <button class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</div>
@stop

