@extends('adminlte::page')
@section('title', 'Data Produk')
@section('content_header')
<h1>Data Produk</h1>
@stop
@section('content')
<div class="row">
  <div class="col-12">
    <div class="form-group">
      <a href="{{ url('admin/products/add') }}" class="btn btn-primary">Tambah Data</a>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @elseif($message = Session::get('danger'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @endif

    @php
    $heads = [
    'No',
    'Nama',
    'Harga',
    'Kategori',
    'Gambar',
    ['label' => 'Actions', 'no-export' => true, 'width' => 25],
    ];
    @endphp
    <x-adminlte-datatable id="table3" :heads="$heads" striped hoverable>
    @foreach($data as $produk)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td><a href="javascript:void(0);" class="product-link" data-id="{{ $produk->id }}">{{ $produk->nama_produk }}</a></td>
        <td>Rp {{ number_format($produk->harga,0,",",".") }}</td>
        <td>{{ $produk->kategori->nama_kategori }}</td>
        <td><img src="{{ $produk->gambar }}" alt="{{ $produk->nama_produk }}" width="100"></td>
        <td><a href="{{ url('admin/products/edit/'.$produk->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" onclick="return remove({{ $produk->id }});" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Hapus</a>
      </tr>
    @endforeach
    </x-adminlte-datatable>
  </div>
</div>

<x-adminlte-modal id="modalProduct" theme="red"
    icon="fas fa-motorcycle" size='lg' disable-animations>
    <div id="productDetails"></div>
</x-adminlte-modal>
@stop
@section('js')
<script>
function remove(e){
	Swal.fire({
	title: 'Hapus ?',
	text: "Jika dihapus, data tidak bisa dikembalikan!",
	icon: 'warning',
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	confirmButtonText: 'Ya, hapus !!',
	cancelButtonText: 'Batal',
	container: 'container-class'
	})
	.then((result) => {
		if (result.value) {
			window.location.href = "{{ url('admin/products/delete') }}/"+e ;
		}
	})
}

$(document).ready(function() {
    $('.product-link').click(function() {
        var productId = $(this).data('id');
        
        $.ajax({
            url: "{{ url('admin/products') }}/" + productId,
            method: 'GET',
            success: function(data) {
                $('#productDetails').html(data);
                $('#modalProduct').modal('show');
            },
            error: function(error) {
                console.log(error);
                alert('Gagal mengambil data produk.');
            }
        });
    });
});
</script>
@stop