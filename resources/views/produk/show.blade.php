<div class="row">
    <div class="col-12">
        <h4>{{ $produk->nama_produk }}</h4>
        <p>Harga: Rp {{ number_format($produk->harga,0,",",".") }}</p>
        <p>Kategori: {{ $produk->kategori->nama_kategori }}</p>
        <p>Spesifikasi: <br>{!! $produk->spesifikasi !!}</p> 
        <img src="{{ $produk->gambar }}" alt="{{ $produk->nama_produk }}" width="200">
    </div>
</div>