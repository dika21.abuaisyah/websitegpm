@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
<div class="row">
  <div class="col-lg-3 col-6">
    <div class="small-box bg-info">
      <div class="inner">
        <h3>{{ $ju }}</h3>
        <p>User</p>
      </div>
      <div class="icon">
        <i class="fas fa-user"></i>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <div class="small-box bg-success">
      <div class="inner">
        <h3>{{ $jp }}</h3>
        <p>Produk</p>
      </div>
      <div class="icon">
        <i class="fas fa-motorcycle"></i>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <div class="small-box bg-warning">
      <div class="inner">
        <h3>{{ $jb }}</h3>
        <p>Berita</p>
      </div>
      <div class="icon">
        <i class="fas fa-newspaper"></i>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <div class="small-box bg-danger">
      <div class="inner">
        <h3>{{ $jg }}</h3>
        <p>Galeri</p>
      </div>
      <div class="icon">
        <i class="fas fa-image"></i>
      </div>
    </div>
  </div>
</div>
@stop