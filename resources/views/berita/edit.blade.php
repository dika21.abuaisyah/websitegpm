@extends('adminlte::page')

@section('title', 'Edit Data Berita')

@section('content_header')
<h1> Edit Data Berita</h1>
@stop

@section('content')
@if ($message = Session::get('danger'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif
<a href="{{ url('/admin/news') }}" class="btn btn-warning mb-3"><i class="fa fa-arrow-left"></i> Kembali</a>
<div class="card col-12 col-md-6">
    <div class="card-body">
        <form action="{{ route('news/update',['id'=>$berita->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="kategori_id">Kategori Berita<span class="text-danger">*</span></label>
                <x-adminlte-select2 name="kategori_id"
                    igroup-size="md" data-placeholder="Select an option...">
                    @foreach($kategori as $k)
                    <option value="{{ $k->id }}" @if($berita->kategori_id == $k->id) selected @endif>{{ $k->nama_kategori }}</option>
                    @endforeach
                </x-adminlte-select2>
                @if($errors->has('kategori_id'))
                <small class="text-danger">{{ $errors->first('kategori_id') }}</small>
                @endif                
            </div>
            <div class="form-group">
                <label for="judul">Judul Berita<span class="text-danger">*</span></label>
                <input id="judul" class="form-control @if($errors->has('judul')) is-invalid @endif" type="text" name="judul" value="{{ $berita->judul ?? old('judul') }}" placeholder="Judul Berita" required>
                @if($errors->has('judul'))
                <small class="text-danger">{{ $errors->first('judul') }}</small>
                @endif
            </div>
            <div class="form-group">
                <label for="thumbnail">Thumbnail Berita<span class="text-danger">*</span></label>
                <input id="thumbnail" class="form-control @if($errors->has('thumbnail')) is-invalid @endif" type="file" name="thumbnail" value="{{ old('thumbnail') }}" placeholder="Thumbnail Berita" required>
                @if($errors->has('thumbnail'))
                <small class="text-danger">{{ $errors->first('thumbnail') }}</small>
                @endif
            </div>
            @php
            $config = [
                "height" => "150"
            ];
            @endphp
            <div class="form-group">
                <label for="isi">Isi Berita</label>
                <x-adminlte-text-editor name="isi" :config="$config" placeholder="Isi Berita">{{ $berita->isi }}</x-adminlte-text-editor>
                @if($errors->has('isi'))
                <small class="text-danger">{{ $errors->first('isi') }}</small>
                @endif
            </div>
            <div class="form-group">
                <button class="btn btn-primary">Edit</button>
            </div>
        </form>
    </div>
</div>
@stop

