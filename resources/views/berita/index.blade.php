@extends('adminlte::page')
@section('title', 'Data Berita')
@section('content_header')
<h1>Data Berita</h1>
@stop
@section('content')
<div class="row">
  <div class="col-12">
    <div class="form-group">
      <a href="{{ url('admin/news/add') }}" class="btn btn-primary">Tambah Data</a>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @elseif($message = Session::get('danger'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @endif

    @php
    $heads = [
    'No',
    'Judul',
    'Kategori',
    'Thumbnail',
    ['label' => 'Actions', 'no-export' => true, 'width' => 25],
    ];
    @endphp
    <x-adminlte-datatable id="table3" :heads="$heads" striped hoverable>
    @foreach($data as $berita)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td><a href="javascript:void(0);" class="news-link" data-id="{{ $berita->id }}">{{ $berita->judul }}</a></td>
        <td>{{ $berita->kategori->nama_kategori }}</td>
        <td><img src="{{ $berita->thumbnail }}" alt="{{ $berita->judul }}" width="100"></td>
        <td><a href="{{ url('admin/news/edit/'.$berita->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" onclick="return remove({{ $berita->id }});" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Hapus</a>
      </tr>
    @endforeach
    </x-adminlte-datatable>
  </div>
</div>

<x-adminlte-modal id="modalNews" theme="red"
    icon="fas fa-newspaper" size='lg' disable-animations>
    <div id="newsDetails"></div>
</x-adminlte-modal>
@stop
@section('js')
<script>
function remove(e){
	Swal.fire({
	title: 'Hapus ?',
	text: "Jika dihapus, data tidak bisa dikembalikan!",
	icon: 'warning',
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	confirmButtonText: 'Ya, hapus !!',
	cancelButtonText: 'Batal',
	container: 'container-class'
	})
	.then((result) => {
		if (result.value) {
			window.location.href = "{{ url('admin/news/delete') }}/"+e ;
		}
	})
}

$(document).ready(function() {
    $('.news-link').click(function() {
        var newsId = $(this).data('id');
        
        $.ajax({
            url: "{{ url('admin/news') }}/" + newsId,
            method: 'GET',
            success: function(data) {
                $('#newsDetails').html(data);
                $('#modalNews').modal('show');
            },
            error: function(error) {
                console.log(error);
                alert('Gagal mengambil data berita.');
            }
        });
    });
});
</script>
@stop