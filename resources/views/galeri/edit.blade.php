@extends('adminlte::page')

@section('title', 'Edit Data Galeri')

@section('content_header')
<h1> Edit Data Galeri</h1>
@stop

@section('content')
@if ($message = Session::get('danger'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif
<a href="{{ url('/admin/gallery') }}" class="btn btn-warning mb-3"><i class="fa fa-arrow-left"></i> Kembali</a>
<div class="card col-12 col-md-6">
    <div class="card-body">
        <form action="{{ route('gallery/update',['id'=>$galeri->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="judul">Judul Galeri<span class="text-danger">*</span></label>
                <input id="judul" class="form-control @if($errors->has('judul')) is-invalid @endif" type="text" name="judul" value="{{ $galeri->judul ?? old('judul') }}" placeholder="Judul Galeri" required>
                @if($errors->has('judul'))
                <small class="text-danger">{{ $errors->first('judul') }}</small>
                @endif
            </div>
            <div class="form-group">
                <label for="gambar">Gambar Galeri<span class="text-danger">*</span></label>
                <input id="gambar" class="form-control @if($errors->has('gambar')) is-invalid @endif" type="file" name="gambar" value="{{ old('gambar') }}" placeholder="Gambar Galeri" required>
                @if($errors->has('gambar'))
                <small class="text-danger">{{ $errors->first('gambar') }}</small>
                @endif
            </div>
            <div class="form-group">
                <button class="btn btn-primary">Edit</button>
            </div>
        </form>
    </div>
</div>
@stop

