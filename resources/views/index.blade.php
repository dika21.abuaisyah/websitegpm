@extends('layouts.front')
@section('content')
<header id="header" class="header d-flex align-items-center fixed-top">
  <div class="container-fluid container-xl position-relative d-flex align-items-center">
    <a href="{{ url('/') }}" class="logo d-flex align-items-center me-auto">
      <h1 class="sitename">PT Gratia Plenamas Motor</h1>
    </a>
    <nav id="navmenu" class="navmenu">
      <ul>
        <li><a href="#hero" class="">Home</a></li>
        <li><a href="#about">About</a></li>
        <li><a href="#product">Product</a></li>
        <li><a href="#news">News</a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>
      <i class="mobile-nav-toggle d-xl-none bi bi-list"></i>
    </nav>
  </div>
</header>
<main class="main">
  <!-- Hero Section -->
  <section id="hero" class="hero section">
    <div class="container">
      <div class="row gy-4">
        <div class="col-lg-6 order-2 order-lg-1 d-flex flex-column justify-content-center" data-aos="zoom-out">
          <h1 class="">Selamat Datang</h1>
          <p class="">Website Company Profile PT Gratia Plenamas Motor</p>
          <div class="d-flex">
            <a href="#about" class="btn-get-started">Get Started</a>
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-out" data-aos-delay="200">
          <img src="{{ asset('frontend/img/gpm.jpg') }}" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>
  </section>
  <!-- /Hero Section -->
  
  <!-- About Section -->
  <section id="about" class="about section">
    <!-- Section Title -->
    <div class="container section-title" data-aos="fade-up">
      <h2 class="">Tentang Kami</h2>
    </div>
    <!-- End Section Title -->
    <div class="container">
      <div class="row gy-4">
        <div class="col-lg-6 content" data-aos="fade-up" data-aos-delay="100">
          <h3 class="text-center">Sejarah</h3>
            {!! $profil1->isi !!}
        </div>
        <div class="col-lg-6" data-aos="fade-up" data-aos-delay="200">
        <h3 class="text-center">Visi & Misi</h3>
            {!! $profil2->isi !!}
        </div>
      </div>
    </div>
  </section>
  <!-- /About Section -->
  <!-- Portfolio Section -->
  <section id="product" class="portfolio section">
    <!-- Section Title -->
    <div class="container section-title" data-aos="fade-up">
      <h2>Produk</h2>
      <p>Produk-produk Unggulan PT Gratia Plenamas Motor</p>
    </div>
    <!-- End Section Title -->
    <div class="container">
    <div class="isotope-layout" data-default-filter="*" data-layout="masonry" data-sort="original-order">

      <!-- Portfolio Filters -->
      <ul class="portfolio-filters isotope-filters aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
        <li data-filter="*" class="filter-active">All</li>
        @foreach($kategori as $category)
          <li data-filter=".filter-{{ $category->id }}">{{ $category->nama_kategori }}</li>
        @endforeach
      </ul><!-- End Portfolio Filters -->

      <!-- Portfolio Items -->
      <div class="row portfolio-items isotope-container aos-init aos-animate" data-aos="fade-up" data-aos-delay="200" style="position: relative;">

        @foreach($produk as $product)
          <div class="col-lg-4 col-md-6 portfolio-item isotope-item filter-{{ $product->kategori_id }}">
            <img src="{{ $product->gambar }}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>{{ $product->nama_produk }}</h4>
              <p>Rp {{ number_format($product->harga, 0, ",", ".") }}</p>
              <a href="{{ $product->gambar }}" data-gallery="portfolio-gallery-{{ $product->kategori_id }}" class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
              <a href="#" title="More Details" class="details-link" data-bs-toggle="modal" data-bs-target="#productModal{{ $product->id }}"><i class="bi bi-link-45deg"></i></a>
              <a href="https://wa.me/6282269066808?text='Saya ingin bertanya tentang {{$product->nama_produk}}'" title="" class="wa-link" target="_blank"><i class="bi bi-whatsapp"></i></a>
            </div>
          </div><!-- End Portfolio Item -->
        @endforeach

      </div><!-- End Portfolio Container -->

    </div>
  </div>

  </section>
  <!-- /Portfolio Section -->

  <section id="news" class="portfolio section">
    <!-- Section Title -->
    <div class="container section-title" data-aos="fade-up">
      <h2>Berita</h2>
      <p>Berita Terbaru PT Gratia Plenamas Motor</p>
    </div>
    <!-- End Section Title -->
    <div class="container">
    <div class="isotope-layout" data-default-filter="*" data-layout="masonry" data-sort="original-order">

      <!-- Portfolio Filters -->
      <ul class="portfolio-filters isotope-filters aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
        <li data-filter="*" class="filter-active">All</li>
        @foreach($kategori_berita as $categoryb)
          <li data-filter=".filter-{{ $categoryb->id }}">{{ $categoryb->nama_kategori }}</li>
        @endforeach
      </ul><!-- End Portfolio Filters -->

      <!-- Portfolio Items -->
      <div class="row portfolio-items isotope-container aos-init aos-animate" data-aos="fade-up" data-aos-delay="200" style="position: relative;">

        @foreach($berita as $news)
          <div class="col-lg-4 col-md-6 portfolio-item isotope-item filter-{{ $news->kategori_id }}">
            <img src="{{ $news->thumbnail }}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>{{ $news->judul }}</h4>
              <a href="{{ $news->thumbnail }}" data-gallery="portfolio-gallery-{{ $news->kategori_id }}" class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
              <a href="#" title="More Details" class="details-link" data-bs-toggle="modal" data-bs-target="#newsModal{{ $news->id }}"><i class="bi bi-link-45deg"></i></a>
            </div>
          </div><!-- End Portfolio Item -->
        @endforeach

      </div><!-- End Portfolio Container -->

    </div>
  </div>

  </section>

  <!-- Contact Section -->
  <section id="contact" class="contact section">
    <!-- Section Title -->
    <div class="container section-title" data-aos="fade-up">
      <h2>Contact</h2>
      <p>Terus berhubungan dengan kami</p>
    </div>
    <!-- End Section Title -->
    <div class="container" data-aos="fade-up" data-aos-delay="100">
      <div class="row gy-4">
        <div class="col-lg-12">
          <form action="{{ url('follow-up/create') }}" method="post">
            @csrf
            <div class="row gy-4">
              <div class="col-md-6">
                <label for="name-field" class="pb-2">Nama</label>
                <input type="text" name="nama" id="name-field" class="form-control" required="">
              </div>
              <div class="col-md-6">
                <label for="email-field" class="pb-2">E-mail</label>
                <input type="email" class="form-control" name="email" id="email-field" required="">
              </div>
              <div class="col-md-12">
                <label for="subject-field" class="pb-2">No WA</label>
                <input type="text" class="form-control" name="no_wa" id="subject-field" required="">
              </div>
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-info">Kirim</button>
              </div>
            </div>
          </form>
        </div>
        <!-- End Contact Form -->
      </div>
    </div>
  </section>
  <!-- /Contact Section -->
</main>

<div class="modal fade" id="productModal{{ $product->id }}" tabindex="-1" aria-labelledby="productModalLabel{{ $product->id }}" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="productModalLabel{{ $product->id }}">{{ $product->nama_produk }}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-12">
            <p>Harga: Rp {{ number_format($product->harga, 0, ",", ".") }}</p>
            <p>Kategori: {{ $product->kategori->nama_kategori }}</p>
            <p>Spesifikasi:</p>
            <p>{!! $product->spesifikasi !!}</p>
            <img src="{{ $product->gambar }}" alt="{{ $product->nama_produk }}" width="200">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="newsModal{{ $news->id }}" tabindex="-1" aria-labelledby="newsModalLabel{{ $news->id }}" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newsModalLabel{{ $news->id }}">{{ $news->nama_produk }}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-12">
            <h3>{{ $news->judul }}</h3>
            <p>{!! $news->isi !!}</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@stop

@section('js')
<script>
  // Script to load product details via AJAX
  $(document).ready(function() {
    $('.details-link').click(function() {
      var productId = $(this).data('product-id');
      $.ajax({
        url: "{{ route('products.detail', ['id' => ':id']) }}".replace(':id', productId),
        method: 'GET',
        success: function(response) {
          $('#productDetail' + productId).html(response);
        },
        error: function(xhr) {
          console.log(xhr.responseText);
        }
      });
    });
  });
</script>

<script>
  // Script to load news details via AJAX
  $(document).ready(function() {
    $('.details-news-link').click(function() {
      var newsId = $(this).data('news-id');
      $.ajax({
        url: "{{ route('news.detail', ['id' => ':id']) }}".replace(':id', newsId),
        method: 'GET',
        success: function(response) {
          $('#newsDetail' + newsId).html(response);
        },
        error: function(xhr) {
          console.log(xhr.responseText);
        }
      });
    });
  });
</script>
@stop