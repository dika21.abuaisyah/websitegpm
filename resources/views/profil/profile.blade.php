@extends('adminlte::page')

@section('title', 'Data Profil Perusahaan')

@section('content_header')
<h1> Data Profil Perusahaan</h1>
@stop

@section('content')
@if ($message = Session::get('success'))
  <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
  </div>
@elseif($message = Session::get('danger'))
  <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
  </div>
@endif
<div class="card col-12">
    <div class="card-body">
        <form action="{{ url('/admin/company-profile') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="judul">Profil Perusahaan<span class="text-danger">*</span></label>
                <input id="judul" class="form-control @if($errors->has('judul')) is-invalid @endif" type="text" name="judul" value="{{ $profil->judul ?? old('judul') }}" placeholder="Profil Perusahaan" required disabled>
                @if($errors->has('judul'))
                <small class="text-danger">{{ $errors->first('judul') }}</small>
                @endif
            </div>
            @php
            $config = [
                "height" => "150"
            ];
            @endphp
            <div class="form-group">
                <label for="isi">Isi</label>
                <x-adminlte-text-editor name="isi" :config="$config" placeholder="Isi">{{ $profil->isi ?? old('isi') }}</x-adminlte-text-editor>
                @if($errors->has('isi'))
                <small class="text-danger">{{ $errors->first('isi') }}</small>
                @endif
            </div>
            <div class="form-group">
                <button class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</div>
@stop

