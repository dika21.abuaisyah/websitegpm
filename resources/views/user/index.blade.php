@extends('adminlte::page')
@section('title', 'Data Users')
@section('content_header')
<h1>Data Users</h1>
@stop
@section('content')
<div class="row">
  <div class="col-12">
    @php
    $heads = [
    'No',
    'Nama',
    'E-mail',
    ['label' => 'Actions', 'no-export' => true, 'width' => 5],
    ];
    @endphp
    <x-adminlte-datatable id="table1" :heads="$heads" striped hoverable>
    @foreach($data as $user)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td><a href="{{ url('admin/user/edit/'.$user->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
      </tr>
    @endforeach
    </x-adminlte-datatable>
  </div>
</div>
@stop
@section('js')
<script>
function remove(e){
	Swal.fire({
	title: 'Hapus ?',
	text: "Jika dihapus, data tidak bisa dikembalikan!",
	icon: 'warning',
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	confirmButtonText: 'Ya, hapus !!',
	cancelButtonText: 'Batal',
	container: 'container-class'
	})
	.then((result) => {
		if (result.value) {
			window.location.href = "{{ url('admin/user/delete') }}/"+e ;
		}
	})
}
</script>
@stop