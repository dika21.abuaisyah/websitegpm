@extends('adminlte::page')
@section('title', 'Data Follow Up')
@section('content_header')
<h1>Data Follow Up</h1>
@stop
@section('content')
<div class="row">
  <div class="col-12">
    @php
    $heads = [
    'No',
    'Nama',
    'E-mail',
    ['label' => 'Actions', 'no-export' => true, 'width' => 5],
    ];
    @endphp
    <x-adminlte-datatable id="table1" :heads="$heads" striped hoverable>
    @foreach($data as $fu)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $fu->nama }}</td>
        <td>{{ $fu->email }}</td>
        <td><a href="https://wa.me/{{ $fu->no_wa }}" target="_blank" class="btn btn-sm btn-success"><i class="fab fa-whatsapp"></i> Chat</a>
      </tr>
    @endforeach
    </x-adminlte-datatable>
  </div>
</div>
@stop