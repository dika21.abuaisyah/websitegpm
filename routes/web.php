<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', [App\Http\Controllers\FrontController::class, 'index']);
Route::post('/follow-up/create', [App\Http\Controllers\FrontController::class, 'followup']);

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::get('product/detail/{id}', [App\Http\Controllers\ProdukController::class, 'detail'])->name('products.detail');

Route::get('news/detail/{id}', [App\Http\Controllers\BeritaController::class, 'detail'])->name('news.detail');

Route::group(['prefix' => '/admin'], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::group(['prefix' => '/users'], function () {
        Route::get('/', [App\Http\Controllers\UserController::class, 'index']);
    });

    Route::group(['prefix' => '/products'], function () {
        Route::get('/', [App\Http\Controllers\ProdukController::class, 'index'])->name('products');
        Route::get('add', [App\Http\Controllers\ProdukController::class, 'add'])->name('products/add');
        Route::post('create', [App\Http\Controllers\ProdukController::class, 'create'])->name('products/create');
        Route::get('edit/{id}', [App\Http\Controllers\ProdukController::class, 'edit'])->name('products/edit');
        Route::post('update/{id}', [App\Http\Controllers\ProdukController::class, 'update'])->name('products/update');
        Route::get('delete/{id}', [App\Http\Controllers\ProdukController::class, 'delete'])->name('products/delete');
        Route::get('{id}', [App\Http\Controllers\ProdukController::class, 'show'])->name('products');
    });

    Route::group(['prefix' => '/product-categories'], function () {
        Route::get('/', [App\Http\Controllers\KategoriProdukController::class, 'index'])->name('product-categories');
        Route::get('add', [App\Http\Controllers\KategoriProdukController::class, 'add'])->name('product-categories/add');
        Route::post('create', [App\Http\Controllers\KategoriProdukController::class, 'create'])->name('product-categories/create');
        Route::get('edit/{id}', [App\Http\Controllers\KategoriProdukController::class, 'edit'])->name('product-categories/edit');
        Route::post('update/{id}', [App\Http\Controllers\KategoriProdukController::class, 'update'])->name('product-categories/update');
        Route::get('delete/{id}', [App\Http\Controllers\KategoriProdukController::class, 'delete'])->name('product-categories/delete');
    });

    Route::group(['prefix' => '/news-categories'], function () {
        Route::get('/', [App\Http\Controllers\KategoriBeritaController::class, 'index'])->name('news-categories');
        Route::get('add', [App\Http\Controllers\KategoriBeritaController::class, 'add'])->name('news-categories/add');
        Route::post('create', [App\Http\Controllers\KategoriBeritaController::class, 'create'])->name('news-categories/create');
        Route::get('edit/{id}', [App\Http\Controllers\KategoriBeritaController::class, 'edit'])->name('news-categories/edit');
        Route::post('update/{id}', [App\Http\Controllers\KategoriBeritaController::class, 'update'])->name('news-categories/update');
        Route::get('delete/{id}', [App\Http\Controllers\KategoriBeritaController::class, 'delete'])->name('news-categories/delete');
    });

    Route::group(['prefix' => '/news'], function () {
        Route::get('/', [App\Http\Controllers\BeritaController::class, 'index'])->name('news');
        Route::get('add', [App\Http\Controllers\BeritaController::class, 'add'])->name('news/add');
        Route::post('create', [App\Http\Controllers\BeritaController::class, 'create'])->name('news/create');
        Route::get('edit/{id}', [App\Http\Controllers\BeritaController::class, 'edit'])->name('news/edit');
        Route::post('update/{id}', [App\Http\Controllers\BeritaController::class, 'update'])->name('news/update');
        Route::get('delete/{id}', [App\Http\Controllers\BeritaController::class, 'delete'])->name('news/delete');
        Route::get('{id}', [App\Http\Controllers\BeritaController::class, 'show'])->name('news');
    });

    Route::group(['prefix' => '/gallery'], function () {
        Route::get('/', [App\Http\Controllers\GaleriController::class, 'index'])->name('gallery');
        Route::get('add', [App\Http\Controllers\GaleriController::class, 'add'])->name('gallery/add');
        Route::post('create', [App\Http\Controllers\GaleriController::class, 'create'])->name('gallery/create');
        Route::get('edit/{id}', [App\Http\Controllers\GaleriController::class, 'edit'])->name('gallery/edit');
        Route::post('update/{id}', [App\Http\Controllers\GaleriController::class, 'update'])->name('gallery/update');
        Route::get('delete/{id}', [App\Http\Controllers\GaleriController::class, 'delete'])->name('gallery/delete');
        Route::get('{id}', [App\Http\Controllers\GaleriController::class, 'show'])->name('gallery');
    });

    Route::group(['prefix' => '/company-profile'], function () {
        Route::get('/', [App\Http\Controllers\ProfilController::class, 'index_profile'])->name('company-profile');
        Route::post('/', [App\Http\Controllers\ProfilController::class, 'update_profile'])->name('company-profile');
    });

    Route::group(['prefix' => '/vision-mission'], function () {
        Route::get('/', [App\Http\Controllers\ProfilController::class, 'index_visi'])->name('vision-mission');
        Route::post('/', [App\Http\Controllers\ProfilController::class, 'update_visi'])->name('vision-mission');
    });

    Route::group(['prefix' => '/follow-up'], function () {
        Route::get('/', [App\Http\Controllers\FollowUpController::class, 'index'])->name('follow-up');
    });

});